<?php


?>
<div class="container">
	<legend></legend>
	<blockquote class="blockquote-reverse">
		Created by the <kbd>1337guard</kbd>
		<footer>
			Powered by <a href="http://www.battlelog.com" target="_blank">BattleLog</a>
			<br>
			Charts from <a href="http://www.highcharts.com" target="_blank">Highcharts</a>
			<br>
			CSS from <a href="http://www.getbootstrap.com" target="_blank">Bootstrap</a>
			<br>
			Bootstrap theme from <a href="http://bootswatch.com" target="_blank">Bootswatch</a>
			<br>
		</footer>
	</blockquote>
</div>
</body>
</html>

<?php


?>