<?php

final class config {
	private $preferences;

	private function __construct() {
		$this->counter = $this->counter + 1;
		$domain = "http://battlelog.battlefield.com";
		$this->preferences = array();

		$this->preferences['path'] = array();
		$this->preferences['path']['cacheFilePath'] = '../cachedReport/';

		$this->preferences['battlelog'] = array();
		$this->preferences['battlelog']['reportOverview'] = $domain . '/bf4/warsawbattlereportspopulate/' . '#personaId#' . '/2048/' . '#platform#' . '/';
		$this->preferences['battlelog']['reportDetails'] = $domain . '/bf4/battlereport/loadgeneralreport/' . '#reportId#' . '/' . '#platform#' . '/' . '#personaId#';
		$this->preferences['battlelog']['generalStats'] = $domain . '/bf4/warsawdetailedstatspopulate/' . '#personaId#' . '/' . '#platform#' . '/';
		$this->preferences['battlelog']['playerOverview'] = $domain . '/bf4/soldier' . '/' . '#personaName#' . '/stats/' . '#personalId#' . '/' . '#platformAsText#' . '/';

	}
	private function __clone() {}
	private function __wakeup() {}

	public static function getInstance() {
		static $instance;
		if (!isset($instance)) {
			$instance = new config();
		}
		return $instance;
	}

	public function getCacheReportPath() {
		return $this->preferences['path']['cacheFilePath'];
	}
	
	public function getReportOverview($personaId, $platform) {
		$str = $this->preferences['battlelog']['reportOverview'];
		$str = str_replace('#personaId#', $personaId, $str);
		$str = str_replace('#platform#', $platform, $str);
		return $str;
	}

	public function test() {
		return "Testing...<br>";
	}
}
?>