<?php


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Advanced Battlelog</title>
	<link href="../lib/battlelog/css/bootdark.min.css" rel="stylesheet">
	<script src="../lib/jquery-1.12.3.min.js"></script>
	<script src="../lib/jquery-ui-1.11.4.js"></script>
	<script src="../lib/battlelog/js/highcharts/highcharts.src.js"></script>
	<script src="../lib/battlelog/js/highcharts/highcharts-more.src.js"></script>
	<script src="../lib/battlelog/js/highcharts/modules/solid-gauge.src.js"></script>
	<script src="../lib/battlelog/js/highcharts/modules/data.src.js"></script>
	<script src="../lib/battlelog/js/highcharts/dark-unica-theme.js"></script>
	<script src="../lib/battlelog/js/bootstrap.min.js"></script>
	<script src="lib/js/battlelog.js"></script>
</head>
<body>


	<?php

	?>