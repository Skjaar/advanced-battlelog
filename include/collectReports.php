<?php

final class collectReports {
	private static $collectReports;

	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	public static function getInstance() {
		if (null === static::$collectReports) {
			static::$collectReports = new static();
		}

		return static::$collectReports;
	}
	
	public function collectReports($reportList) {
		
		require_once('cacheReader.php');
		$cacheReader = CacheReader::getInstance();

		for ($i = 0; $i < sizeof($reportList['gameReports']); $i++) { 
			$cachePath = '../cachedReport/' . $platform . '/' . gmdate("Y-m-d",$reportList['gameReports'][$i]['createdAt']) . "/" . $reportList['gameReports'][$i]['gameReportId'] . "/";
			
			if (!file_exists($cachePath)) {
				mkdir($cachePath, 0777, true);
			}

			if (!file_exists($cachePath . $personaID . ".txt")) {
				$reportContent = file_get_contents('http://battlelog.battlefield.com/bf4/battlereport/loadgeneralreport/' . $reportList['gameReports'][$i]['gameReportId'] . '/' . $platform . '/' . $personaID . '');
				$cachedReport = fopen($cachePath . $personaID . ".txt", "w");
				fwrite($cachedReport, $reportContent);
				fclose($cachedReport);
				$reportList['gameReports'][$i]['detailedReport'] = json_decode($reportContent, true);
			} else {
				$reportList['gameReports'][$i]['detailedReport'] = $cacheReader->readFromCache($cachePath . $personaID . ".txt");
				$reportList['gameReports'][$i]['size'] = round(filesize($cachePath . $personaID . ".txt") / 1024);
				$reportList['gameReports'][$i]['cached'] = "true";
			}
		}

		
	}

}