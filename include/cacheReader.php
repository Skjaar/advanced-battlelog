<?php

final class CacheReader {


	private static $cacheReader;

	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	public static function getInstance() {
		if (null === static::$cacheReader) {
			static::$cacheReader = new static();
		}

		return static::$cacheReader;
	}
	/**
	* Takes path to cached file and returns content as array structure formated data.
	*/
	public function readFromCache($pathToCacheFile) {
		if(file_exists($pathToCacheFile)) {
			$cachedContent = json_decode(file_get_contents($pathToCacheFile), true);
			$cachedContent['status'] = 'Success';
			$cachedContent['size'] = round(filesize($pathToCacheFile)) . ' bytes';
			return $cachedContent;	
		} else {
			return json_decode('{"status" : "File does not exist."}', true);
		}
		
	}


}