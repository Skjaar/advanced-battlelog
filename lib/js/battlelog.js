/**
	Takes a number that you wants to display and a break point from which the colour should be calculated from.
	*/
	function colourNumbers(number, breakPoint){
		if (number < breakPoint) {
			return '<div class="text-danger">' + number + '</div>';
		}
		if (number > breakPoint) {
			return '<div class="text-success">' + number + '</div>';
		}
		return number;
	}

/**
	Returns a number with initialing zeros. (1 -> 01)
	*/
	function pad(num, size){
		if (num.toString().length <= size){
			return ('000000000' + num).substr(-size);
		}
		return num;
	}
/**
	Takes unix time and returns a read friendly string as YYYY-MM-DD HH:MM
	*/
	function getTimeAndDate(unixTime){
		var date = new Date(unixTime * 1000);
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var hours = date.getHours();
		var minutes = "0" + date.getMinutes();
		var formattedTime = (hours < 10 ? "0" + hours : hours) + ':' + minutes.substr(minutes.length-2);
		return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + " " + formattedTime;
	}
/**
	Takes unix time and returns how many hours it is with minutes included.
	*/
	function getTimeInHours(unixTime){
		var hours = (unixTime/60/60).toString();
		hours = hours.split(".");
		var minutes = ((unixTime - hours[0]*60*60)/60).toString();
		minutes = minutes.split(".");

		return hours[0] + "h" + minutes[0] + "m";
	}

	function getTimeInMinutes(unixTime) {
		var date = new Date(unixTime * 1000);
		var minutes = (unixTime/60).toPrecision(2);	
		var seconds = (unixTime/60).toString().split('.');
		return minutes + 'm' + date.getSeconds() + 's';
	}

	function noNullValue(value) {
		return value != null ? value : 0;
	}

	function getAceSquadStar(rowData) {
		for (var i = 0; i < rowData.detailedReport.playerReport.unlocks.awards.length; i++) {
			var unlockId = rowData.detailedReport.playerReport.unlocks.awards[i].unlockId;
			if (unlockId == 'r08') {
				return '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>';	
			}
		}
		return '';
	}

	function getUnlockIdAsPrettyText(unlockId) {
		var returnRibbon;
		switch(unlockId.substr(1,2)){
			case '01':
			returnRibbon = 'Kill Assist';
			break;
			case '02':
			returnRibbon = 'Anti-vehicle';
			break;
			case '03':
			returnRibbon = 'Squad wipe';
			break;
			case '04':
			returnRibbon = 'Headshot';
			break;
			case '05':
			returnRibbon = 'Avenger';
			break;
			case '06':
			returnRibbon = 'Savior';
			break;
			case '07':
			returnRibbon = 'Spotting';
			break;
			case '08':
			returnRibbon = 'Ace Squad';
			break;
			case '09':
			returnRibbon = 'MVP';
			break;
			case '10':
			returnRibbon = 'Handgun';
			break;
			case '11':
			returnRibbon = 'Assault Rifle';
			break;
			case '12':
			returnRibbon = 'Carbine';
			break;
			case '13':
			returnRibbon = 'Sniper Rifle';
			break;
			case '14':
			returnRibbon = 'LMG';
			break;
			case '15':
			returnRibbon = 'DMR';
			break;
			case '16':
			returnRibbon = 'PWD';
			break;
			case '17':
			returnRibbon = 'Shotgun';
			break;
			case '18':
			returnRibbon = 'Melee';
			break;
			case '19':
			returnRibbon = 'Infantry Fighting Vehicle';
			break;
			case '20':
			returnRibbon = 'Main Battle Tank';
			break;
			case '21':
			returnRibbon = 'Anti-Air Tank';
			break;
			case '22':
			returnRibbon = 'Scout Helicopter';
			break;
			case '23':
			returnRibbon = 'Attack Helicopter';
			break;
			case '24':
			returnRibbon = 'Jet Fighter';
			break;
			case '25':
			returnRibbon = 'Watercraft';
			break;
			case '26':
			returnRibbon = 'Conquest Flag Capture';
			break;
			case '27':
			returnRibbon = 'M-COM Attacker';
			break;
			case '28':
			returnRibbon = 'Bomb Delivery';
			break;
			case '29':
			returnRibbon = 'Conquest';
			break;
			case '30':
			returnRibbon = 'Rush';
			break;
			case '31':
			returnRibbon = 'Team Deathmatch';
			break;
			case '32':
			returnRibbon = 'Squad Deathmatch';
			break;
			case '33':
			returnRibbon = 'Obliteration';
			break;
			case '34':
			returnRibbon = 'Defuse';
			break;
			case '35':
			returnRibbon = 'Domination';
			break;
			case '36':
			returnRibbon = 'Medkit';
			break;
			case '37':
			returnRibbon = 'Defibrillator';
			break;
			case '38':
			returnRibbon = 'Repair tool';
			break;
			case '39':
			returnRibbon = 'Marksman';
			break;
			case '40':
			returnRibbon = 'Radio Beacon Spawn';
			break;
			case '41':
			returnRibbon = 'Ammo';
			break;
			case '42':
			returnRibbon = 'Commander Surveillance';
			break;
			case '43':
			returnRibbon = 'Commander Resupply';
			break;
			case '44':
			returnRibbon = 'Commander Leadership';
			break;
			case '45':
			returnRibbon = 'Commander Gunship';
			break;
			case '47':
			returnRibbon = 'Radio Beacon Spawn';
			break;
			default:
			return unlockId;
		}
		return returnRibbon + ' ' + (unlockId.substr(0,1) == 'r' ? 'Ribbon' : 'Medal');
	}