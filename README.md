# README #

Consists of two parts. The data collector (PHP) and the client (Javascript). The data collector gets information from DICE Battlelog servers and combines the information. For the report overview list which contains 10 last games it fetches detailed game information for each game. Cacheing is done through saving it as a file and fetched if it exists. 
The clients part is to display a portion of the information available. 

### How do I get set up? ###

To set it up you need:
Web server with PHP running,
Access to files with possibility of granting write rights to folder.