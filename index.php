<?php
include 'include/header.php';
?>
	
	<script type="text/javascript">
	var rowToggler = false;
	var globalUserName;
	var globalUserid;
	var globalLastGame;
	var globalScorePerMinute;
	var globalKillsPerMinute;
	var globalKillDeathRatio;
	var globalPlatform;
	var maps;

	$(function () {
		
		var platform = getUrlVars("platform");
		if (platform != null) {
			var userName = getUrlVars("username");
			var userId = getUrlVars("userid");
			if (userId != null) {
				getData(userId, platform);
			} else if (userName != null) {
				//getData(getUserIdFromUserName(userName), platform);
			}
		}

		$.getJSON( "services/json/maps.json", function( data ) {
			maps = data;
		});
	});

	$(document).ready(function(){
		$("#battlelog_username" ).keyup(function() {
			showUsersFromBattlelogWithName($('#battlelog_username').val());
		})
	});

	function getUrlVars(key) {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,arrayKey,value) {
			vars[arrayKey] = value;
		});
		return key == null ? vars : vars[key];
	}

	function handleReports(theData) {
		console.log(theData);
		if (theData.data.messageSID != null && theData.data.messageSID == 'ID_WEB_PROFILE_NO_STATS') {
			$("#result").html("<h1>Oh no!! You got no multiplayer stats to show! You anti social excuse for a gamer!!!</h1>");
			hideLoadingSign();
			return;
		}
		rowCounter = 0
		populateDashboard(theData.data.generalStats);
		globalUserName = theData.data.personaName;
		$("#main_header_title").html('<h3>Advanced Battlelog report ' + '<span class="label label-success">'  + globalUserName +  '</span>' + '</h3>');
		setTable();
		if (theData.data.overview.context.club != null) {
			var platoonLink = '<a href="http://1337guard.com/battlelog/platoon_overview.php?platoonid=' + theData.data.overview.context.club.id + '" target="_blank">Platoon view</a>';
			$("#platoon_link").html(platoonLink);
		}
		globalScorePerMinute = theData.data.generalStats.scorePerMinute;
		globalkillsPerMinute = theData.data.generalStats.killsPerMinute;
		globalKillDeathRatio = ((1 * theData.data.generalStats.kills)/(1 * theData.data.generalStats.deaths)).toPrecision(2);
		skillOverTime = theData.data.generalStats.skill * 1;
		populateTable(theData.data.gameReports);
		calculateAvg();
		hideLoadingSign();
		updateCharts();
		$("#ExtraStatButtons").show();
	}

	function getPlatoonLink(theData) {
		return theData.data.overview.context.club.id;
	}

	function updateCharts() {

		// K/D Ratio
		$('#kdChartContainer').highcharts({
			data: {
				csv: $("#csv_ratio_chart").html()
			},
			legend: {
				enabled: false
			},
			title: {
				text: 'Kill/Death ratio'
			},
			xAxis: {
				lineWidth: 0,
				minorGridLineWidth: 0,
				lineColor: 'transparent',
				reversed: true,
				minTickInterval: 1
			},
			series: [{
				type: 'areaspline',
				color: '#5679c4',
				negativeColor: '#c4392d',
				fillOpacity: 0.5,
				threshold: globalKillDeathRatio
			}]	
		});

		// Kills and deaths
		$('#killsDeathsChartContainer').highcharts({
			chart: {
				type: 'line'
			},
			data: {
				csv: $("#csv_kills_and_deaths_chart").html()
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				}
			},
			tooltip: {
				shared: true,
			},
			legend: {
				enabled: true
			},
			title: {
				text: 'Kills and Deaths'
			},
			xAxis: {
				reversed: true,
				minTickInterval: 1
			},
			yAxis: [{
				min: 0
			}, {
				linkedTo: 0,
				opposite: true
			}],
			series: [{
				color: '#5679c4'
			},{
				color: '#c4392d'
			}]	
		});

		//Score per minute
		$('#spmChartContainer').highcharts({
			chart: {
				type: 'line'
			},
			legend: {
				enabled: false
			},
			data: {
				csv: $("#csv_spm_chart").html()
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				}
			},
			title: {
				text: 'Score per minute'
			},
			xAxis: {
				reversed: true,
				minTickInterval: 1
			},
			yAxis: [{
				min: 0
			}, {
				linkedTo: 0,
				opposite: true
			}],
			series: [{
				type: 'areaspline',
				color: '#5679c4',
				negativeColor: '#c4392d',
				threshold: globalScorePerMinute
			}]	
		});
		
		// Skill
		$('#skillChartContainer').highcharts({
			chart: {
				type: 'line'
			},
			legend: {
				enabled: false
			},
			data: {
				csv: $("#csv_skill_chart").html()
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				}
			},
			title: {
				text: 'Skill over time'
			},
			xAxis: {
				reversed: true,
				minTickInterval: 1
			},
			yAxis: [{
				// min: 0
			}, 
			{
				linkedTo: 0,
				opposite: true
			}],
			series: [{
				// type: 'areaspline',
				color: '#F6E825'
				// negativeColor: '#c4392d',
				// threshold: globalScorePerMinute
			}]	
		});

		// Position
		$('#positionChartContainer').highcharts({
			chart: {
				type: 'line'
			},
			legend: {
				enabled: false
			},
			data: {
				csv: $("#csv_position_chart").html()
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: true
				}
			},
			title: {
				text: 'Position'
			},
			xAxis: {
				reversed: true,
				minTickInterval: 1
			},
			yAxis: [{
				reversed: true,
				// min: 0
			}, 
			{
				linkedTo: 0,
				reversed: true,
				opposite: true
			}],
			series: [{
				// type: 'areaspline',
				color: '#F6E825'
				// negativeColor: '#c4392d',
				// threshold: globalScorePerMinute
			}]	
		});

		// Win and loss chart
		$('#winAndLoseChartContainer').highcharts({
			chart: {
				type: 'line'
			},
			data: {
				csv: $("#csv_win_and_lose_chart").html()
			},
			plotOptions: {
				column: {
					dataLabels: {
						enabled: false
					},
					enableMouseTracking: false
				}
			},
			legend: {
				enabled: false
			},
			title: {
				text: 'Win/Lose'
			},
			xAxis: {
				reversed: true,
				minTickInterval: 1
			},
			yAxis: [{
				max: 1,
				min: -1

			}],
			series: [{
				// type: 'areaspline',
				color: '#5679c4',
				negativeColor: '#c4392d',
				lineWidth: 5
			}]	
		});
	}

	function showUsersFromBattlelogWithName(username) {
		$.ajax({
			type: 'get',
			data: {'username' : encodeURIComponent(username)},
			url: 'services/GetPersonaId.php',
			contentType: "application/json",
			accept: "application/json",
			dataType: "json",
			success: showUsers
		})
	}

	function showUsers(data) {
		if (data.error === null) {
			$('#battlelog_personas').html("");
			var someArray = data.personaId;
			for (var prop in someArray) {
				if (someArray.hasOwnProperty(prop)) { 
					$('#battlelog_personas').append(getPrettyBattlelogNames(someArray[prop]));
				}
			}
		} else {
			$('#battlelog_personas').html(data.error.message + "<br><br>");
		}
	}

	function getPrettyBattlelogNames(battlelogData) {
		if (battlelogData == null) {
			return "";
		}
		var personaId = battlelogData.personaId;
		var platform = battlelogData.platform;
		var str = '<a class="btn btn-warning" href="?userid=' + personaId + '&platform=' + platform + '">';
		str += battlelogData.personaName;
		str += " [";
		str += battlelogData.platform;
		str += "] ";
		str += '</a>';
		str += '<br>';
		str += '<br>';
		return str;
	}

	function getData(userid, platform) {
		if(userid == "---" || userid == "" || userid == null  || platform == "" || platform == null) {
			console.log('Error');
			return;
		}
		$("#result").html("");
		$("#csv_kills_and_deaths_chart").html("game,Kills,Deaths\n");
		$("#csv_ratio_chart").html("game,Ratio\n");
		$("#csv_spm_chart").html("game,Score Per Minute\n");
		$("#csv_skill_chart").html("game,Skill\n");
		$("#csv_position_chart").html("game,Position\n");
		$("#csv_win_and_lose_chart").html("game,Win Lose\n");
		
		globalUserid = userid;
		globalPlatform = getPlatformIdFromReadableName(platform);
		showLoadingSign();
		$.ajax({
			type: 'get',
			data: {'userid' : userid,'platform' : globalPlatform},
			url: 'services/player_report.php',
			contentType: "application/json",
			accept: "application/json",
			dataType: "json",
			success: handleReports
		})
	}

	function getPlatformIdFromReadableName(platform) {
		platform = platform.toLowerCase();
		if(platform == 'pc') {
			return 1;
		}
		if(platform == 'ps4') {
			return 32;
		}
		if(platform == 'xbone') {
			return 64
		}
	}

	function getDataOptionList(optionListData) {
		optionListData = optionListData.split(',');
		var userid = optionListData[0];
		var platform = optionListData[1];
		var quickURL = "http://www.1337guard.com/battlelog/?userid=";
		$("#direct_link").html('<small>Quick link: <a href="' + quickURL + userid + '&platform=' + platform + '">' + quickURL + userid + '&platform=' + platform +  '</a></small>');
		getData(userid, platform);
	}


	function populateTable(gameReports) {
		for (var i = 0; i < gameReports.length; i++) {
			addRowToTable(gameReports[i]);
		};
		globalLastGame = gameReports[(gameReports.length - 1)].createdAt;
	}

	var rowCounter = 0;

	function setTable() {
		var result = "";
		result += '<table class="table table-bordered" id="tab_logic">';
		result += '<tr><th></th><th>Game</th><th>Date</th><th>Map</th><th>K/D r</th><th>K</th><th>D</th><th>A</th><th>S</th><th>Acc</th><th>Score</th><th>SPM</th><th>Pos</th><th>Misc.</th></tr>';
		result += '<tr' + (checkRowToggler() ? ' class="active" ' : '') + ' id="row_0"></tr>';
		result += '</table>';
		result += '<div id="showMore"></div>';
		result += '<div class="btn-group btn-group-justified" role="group" aria-label="..."><div class="btn-group" role="group"><button type="button" id="loadMoreReportsButton" onclick="loadMoreReports()" class="btn btn-info">Load more reports...</button></div></div>';
		$("#result").html(result);
	}

	function addRowToTable(rowData) {
		var table = "";
		table += '' +
		'<td ' + (isPlayerOnWinningTeam(rowData) ? 'class="success"' : 'class="danger"') + '>' + '<input id="checked_' + rowCounter + '" type="checkbox" checked="checked" onchange="calculateAvg()" /> ' + pad(rowCounter + 1, 2) + '</td>' +
		'<td ' + (isPlayerOnWinningTeam(rowData) ? 'class="success"' : 'class="danger"') + '><div class="text-center">' + maps[rowData.gameMode] +  ' ' + '</div></td>' +
		'<td id="create_date_' + rowCounter + '">' + '<span class="glyphicon glyphicon-info-sign" aria-hidden="true" data-toggle="collapse" data-target="#extra_info_' + rowCounter + '" aria-expanded="false" aria-controls="collapseExample"></span> ' + createDate(rowData) + '</td>' +
		'<td id="map_' + rowCounter + '">' + getAceSquadStar(rowData) + ' ' + maps[rowData.detailedReport.gameServer.map] + ' ' + getAceSquadStar(rowData) + '</td>' +
		'<td><div id="ratio_' + rowCounter + '" class="text-center">' + getRatioColoured(rowData) + '</div></td>' +
		'<td><div id="kill_' + rowCounter + '" class="text-right">' + getTotalKills(rowData) + '</div></td>' +
		'<td><div id="death_' + rowCounter + '" class="text-right">' + getTotalDeaths(rowData) + '</div></td>' +
		'<td><div id="assist_' + rowCounter + '" class="text-right">' + getTotalAssists(rowData) + '</div></td>' +
		'<td><div id="skill_' + rowCounter + '" class="text-right">' + getSkill(rowData) + ' ' +  '</div></td>' +
		'<td><div id="acc_' + rowCounter + '" class="text-right">' + noNullValue(rowData.detailedReport.playerReport.stats.accuracy) + '%' + '</div></td>' +
		'<td><div id="total_' + rowCounter + '" class="text-right">' + noNullValue(rowData.detailedReport.playerReport.scores.total) + '</div></td>' +
		'<td><div id="spm_' + rowCounter + '" class="text-right">' + getScorePerMinute(rowData) + ' </div></td>' +
		'<td><div id="teamPos_' + rowCounter + '" class="text-right">' + getTeamPosition(rowData) + '</div></td>' +
		'<td id="misc_' + rowCounter + '">' + getMisc(rowData) + '</td>';

		// content in expanded row
		var extraInfo = getExtraInfoContent(rowData); 

		$("#row_" + rowCounter).html(table);
		var extraInfoRow = '<tr id="extra_info_' + rowCounter + '" class="collapse out"><td colspan="14">' + extraInfo + '</td></tr>';
		$('#tab_logic').append(extraInfoRow + '<tr ' + (checkRowToggler() ? ' class="active"' : '') + ' id="row_' + (rowCounter + 1) + '"></tr>');
		$('#csv_ratio_chart').append(pad(rowCounter + 1, 2) + ',' + getRatio(rowData) + '\n');
		$('#csv_kills_and_deaths_chart').append(pad(rowCounter + 1, 2) + ',' + getTotalKills(rowData) + ',' + getTotalDeaths(rowData) + '\n');
		$('#csv_spm_chart').append(pad(rowCounter + 1, 2) + ',' + noNullValue(rowData.detailedReport.playerReport.stats.spm) + '\n');
		$('#csv_skill_chart').append(pad(rowCounter + 1, 2) + ',' + skillOverTime + '\n');
		$('#csv_position_chart').append(pad(rowCounter + 1, 2) + ',' + getTeamPosition(rowData) + '\n');
		skillOverTime -= noNullValue(rowData.detailedReport.playerReport.stats.skill);
		$('#csv_win_and_lose_chart').append(pad(rowCounter + 1, 2) + ',' + (isPlayerOnWinningTeam(rowData) ? 1 : -1) + '\n');
		++rowCounter;
	}

	var skillOverTime;

	function getExtraInfoContent(rowData) {

		var teamStats = getTeamPlayersStats(rowData);
		var table = '';
		
		table += '<div class="row">';
		table += '<div class="col-md-2">';

		table += getExtraInfoContenFirst(rowData);

		table += '</div>';
		table += '<div class="col-md-2">';

		table += getExtraInfoContenSecond(rowData);

		table += '</div>';
		table += '<div class="col-md-2">';

		table += getExtraInfoContenThird(rowData);

		table += '</div>';

		table += '<div class="col-md-2">';

		table += getExtraInfoContenForth(rowData);

		table += '</div>';

		table += '<div class="col-md-2">';

		// table += getExtraInfoContenThird(rowData);

		table += '</div>';

		table += '<div class="col-md-2">';

		// table += getExtraInfoContenThird(rowData);

		table += '</div>';
		//table += '<div class="col-md-3">';

		//table += getExtraInfoContenFirst(rowData);

		//table += '</div>';
		table += '</div>';
		table += '<h4>Awards<small> <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> = Ribbon <span class="glyphicon glyphicon-certificate" aria-hidden="true"></span> = Medal <span class="glyphicon glyphicon-star" aria-hidden="true"></span> = Service Star</small></h4>';
		table += getRibbonsAndAwards(rowData);

		return table;	
		//return "Detailed information about the game here.";	
	}

	function getRibbonsAndAwards(rowData) {
		var toggler = true;
		var ribbons = '';
		for (var i = 0; i < rowData.detailedReport.playerReport.unlocks.awards.length; i++) {
			var unlockId = rowData.detailedReport.playerReport.unlocks.awards[i].unlockId;
			if (unlockId.substr(0,1) == 'r') {
				ribbons += '<span class="label label-info"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> ' + getUnlockIdAsPrettyText(unlockId) + ' ' + rowData.detailedReport.playerReport.unlocks.awards[i].timesTaken + '</span> ';
				toggler = !toggler;
			}
			if (unlockId.substr(0,2) == 'ss') {
				ribbons += '<span class="label label-success"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> ' + unlockId.substr(4, unlockId.length) + '</span> ';
			}
			if (unlockId.substr(0,1) == 'm') {
				ribbons += '<span class="label label-danger"><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span> ' + getUnlockIdAsPrettyText(unlockId) + '</span> ';
			}
		};
		ribbons += '' + (rowData.detailedReport.playerReport.unlocks.battlepacks.length != 0 ? '<span class="label label-warning"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Battlepack ' + rowData.detailedReport.playerReport.unlocks.battlepacks.length + '</span>' : '');
		return ribbons;
	}

	function getExtraInfoContenFirst(rowData) {
		var teamStats = getTeamPlayersStats(rowData);
		table = '';
		table += '<table class="table table-condensed">';
		table += '<tr class="info">';
		table += '<td colspan="2">Combat score</td>'
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Teams: </td>';
		table += '<td>' + teamStats['combatScore'] + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>You: </td>';
		table += '<td>' + rowData.detailedReport.players[globalUserid].combatScore + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>%: </td>';
		table += '<td>' + ((rowData.detailedReport.players[globalUserid].combatScore / teamStats['combatScore']) * 100).toPrecision(3) + '%' + '</td>';
		table += '</tr>';
		table += '</table>';
		return table; 
	}

	function getExtraInfoContenSecond(rowData) {
		var teamStats = getTeamPlayersStats(rowData);
		table = '';
		table += '<table class="table table-condensed">';
		table += '<tr class="info">';
		table += '<td colspan="2">Kills</td>'
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Teams: </td>';
		table += '<td>' + teamStats['kills'] + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>You: </td>';
		table += '<td>' + rowData.detailedReport.players[globalUserid].kills + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>%: </td>';
		table += '<td>' + ((rowData.detailedReport.players[globalUserid].kills / teamStats['kills']) * 100).toPrecision(3) + '%' + '</td>';
		table += '</tr>';
		table += '</table>';
		return table; 
	}

	function getExtraInfoContenThird(rowData) {
		var teamStats = getTeamPlayersStats(rowData);
		table = '';
		table += '<table class="table table-condensed">';
		table += '<tr class="info">';
		table += '<td colspan="2">Deaths</td>'
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Teams: </td>';
		table += '<td>' + teamStats['deaths'] + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>You: </td>';
		table += '<td>' + rowData.detailedReport.players[globalUserid].deaths + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>%: </td>';
		table += '<td>' + ((rowData.detailedReport.players[globalUserid].deaths / teamStats['deaths']) * 100).toPrecision(3) + '%' + '</td>';
		table += '</tr>';
		table += '</table>';
		return table; 
	}

	function getExtraInfoContenForth(rowData) {
		table = '';
		table += '<table class="table table-condensed">';
		table += '<tr class="warning">';
		table += '<td colspan="2">Time</td>'
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Duration: </td>';
		table += '<td>' + getTimeInMinutes(rowData.duration) + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Kill/min</td>';
		table += '<td>' + ((rowData.detailedReport.players[globalUserid].kills/rowData.duration) * 60).toPrecision((rowData.detailedReport.players[globalUserid].kills/rowData.duration) * 60 >= 1 ? 3 : 2) + '</td>';
		table += '</tr>';
		table += '<tr class="active">';
		table += '<td>Death/min</td>';
		table += '<td>' + ((rowData.detailedReport.players[globalUserid].deaths/rowData.duration) * 60).toPrecision((rowData.detailedReport.players[globalUserid].deaths/rowData.duration) * 60 >= 1 ? 3 : 2) + '</td>';
		table += '</tr>';
		table += '</table>';
		return table; 
	}

	function getTeamPlayersStats(rowData) {
		var playerteam = getPlayerTeam(rowData);
		var players = rowData.detailedReport.teams[playerteam].players;
		var stats = {};

		stats['combatScore'] = 0;
		stats['kills'] = 0;
		stats['deaths'] = 0;
		stats['accuracy'] = 0;
		for (var i = 0; i < players.length; i++) {
			stats['combatScore'] += rowData.detailedReport.players[players[i]].combatScore;
			stats['kills'] += rowData.detailedReport.players[players[i]].kills;
			stats['deaths'] += rowData.detailedReport.players[players[i]].deaths;
		};
		return stats;
	}

	function getKillsPerMinute(rowData) {
		var gameTime = (rowData.duration / 60);
		var kills = getTotalKills(rowData);
		return colourNumbers((kills > 0 ? (kills/gameTime).toPrecision(2) : 0), globalkillsPerMinute);
	}

	function getMisc(rowData) {
		return getPrizes(rowData);
	}

	function getScorePerMinute(rowData) {
		return colourNumbers(noNullValue(rowData.detailedReport.playerReport.stats.spm), globalScorePerMinute);
	}

	function getSkill(rowData) {
		return colourNumbers(noNullValue(rowData.detailedReport.playerReport.stats.skill),0);
	}

	function getTotalAssists(rowData) {
		return noNullValue(rowData.detailedReport.playerReport.stats.assists);
	}	

	function getTotalDeaths(rowData) {
		return noNullValue(rowData.detailedReport.playerReport.stats.deaths);
	}

	function getTotalKills(rowData) {
		return noNullValue(rowData.detailedReport.playerReport.stats.kills);
	}

	function getRatioColoured(rowData) {
		return colourNumbers(noNullValue(rowData.detailedReport.playerReport.stats.kd_ratio), globalKillDeathRatio);
	}

	function getRatio(rowData) {
		return noNullValue(rowData.detailedReport.playerReport.stats.kd_ratio);
	}

	function createDate(rowData) {
		return noNullValue(getTimeAndDate(rowData.detailedReport.createdAt));
	}

	function getPrizes(rowData) {
		// Need eye-friendly prize names.
		return (rowData.detailedReport.allPersonalPrizes[globalUserid] != null ? rowData.detailedReport.allPersonalPrizes[globalUserid][0] + ': ' + rowData.detailedReport.allPersonalPrizes[globalUserid][1] : '');
	}

	function showLoadingSign() {
		$("#loadMoreReportsButton").button('loading');
		$("#loadingsign").html('<!--<h3 class="text-center">- Loading content -</h3>--><br><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>');
	}

	function hideLoadingSign() {
		$("#loadingsign").html("");
		$("#loadMoreReportsButton").button('reset');
	}

	function noNullValue(value) {
		return value != null ? value : 0;
	}

	function calculateAvg(){
		var ratioAvg = 0;
		var accAvg = 0;
		var checkedRows = 0;
		var kills = 0;
		var deaths = 0;
		var position = 0;

		for (var i = 0; i < rowCounter; i++) {
			if ($("#checked_" + i).is(":checked")) {
				kill = document.getElementById("kill_" + i).innerHTML;
				death = document.getElementById("death_" + i).innerHTML;
				kills += 1 * kill;
				deaths += 1 * death;
				accuracy = document.getElementById("acc_" + i).innerHTML;
				accAvg += 1 * (accuracy).substring(0, accuracy.length - 1);
				position += 1 * $("#teamPos_" + i).html();
				++checkedRows;
			}
		};
		ratioAvg = (kills/checkedRows)/(deaths/checkedRows);
		accAvg = accAvg/checkedRows;
		position = position/checkedRows;
		$("#ratio_summary").html('' + '' +  "K/D Ratio: "+ '' + ratioAvg.toPrecision(3) + '' + '' );
		$("#kill_death_summary").html('K: ' + kills + '/D: ' + deaths);
		$("#accuracy_summary").html('' + "Accuracy: " + '' + accAvg.toPrecision(2) + '%' + '' );
		$("#position_summary").html('' + "Position: " + '' + position.toPrecision(2) + '' + '' );

	}

	function loadMoreReports() {
		showLoadingSign();

		$.ajax({
			type: 'get',
			data: {	'userid' : globalUserid, 'platform' : globalPlatform, 'lastGame' : globalLastGame},
			url: 'services/player_report_more.php',
			contentType: "application/json",
			accept: "application/json",
			dataType: "json",
			success: handleMoreReports
		})
	}

	function handleMoreReports(theData) {
		console.log(theData);
		populateTable(theData.data.gameReports);
		calculateAvg();
		updateCharts();
		hideLoadingSign();
	}

	function isPlayerOnWinningTeam(gameReport) {
		for (var i = 0; i < gameReport.playerTeams['1'].length; i++) {
			if (gameReport.gameMode == '512') {
				return gameReport.detailedReport.players[globalUserid].gunMasterLevel > 16;
			} 
			if (gameReport.winner == 0) {
				return false; // TODO: fix if a game is a tie. 
			}
			if (gameReport.playerTeams[gameReport.winner][i] == gameReport.player.personaId) {
				return true;
			}
		};
		return false;
	}

	function getPlayerTeam(gameReport) {
		if (gameReport.gameMode == '512') {
			return 1;
		}
		for (var i = 0; i < gameReport.playerTeams['1'].length; i++) {
			if (gameReport.playerTeams['1'][i] == gameReport.player.personaId) {
				return 1;
			}
		};
		return 2;
	}

	function checkRowToggler() {
		rowToggler = !rowToggler
		return rowToggler;
	}

	function getTeamPosition(gameReport) {
		var score = 0;
		if (gameReport.detailedReport.gameMode == '512') { // Gun master
			// for (var key in gameReport.detailedReport.players) {
			// 	if (gameReport.detailedReport.players.hasOwnProperty(key)) {
			// 		if (gameReport.detailedReport.players[key].gunMasterLevel > gameReport.detailedReport.players[globalUserid].gunMasterLevel) {
			// 			++score;
			// 		}
			// 	}
			// }
			return gameReport.detailedReport.players[globalUserid].gunMasterLevel + 'L';
		}
		var playerteam = getPlayerTeam(gameReport);
		var players = gameReport.detailedReport.teams[playerteam].players;
		
		for (var i = 0; i < players.length; i++) {
			if (gameReport.detailedReport.players[players[i]].combatScore > gameReport.detailedReport.players[globalUserid].combatScore) {
				++score;
			}
		};
		return score + 1;
	}

	function populateDashboard(generalStats) {
		$("#kill_death_overall").html("Total K: " + generalStats.kills + "/D: " + generalStats.deaths);
		$("#total_kills").html(generalStats.kills);
		$("#total_deaths").html(generalStats.deaths);
		$("#total_kill_death_diff").html(colourNumbers((1 * generalStats.kills - 1 * generalStats.deaths),0));
		$("#total_kd").html(colourNumbers(((generalStats.kills)/(generalStats.deaths)).toPrecision(4), 1));
		$("#total_accuracy").html(generalStats.accuracy.toPrecision(3) + "%");
		$("#total_headshots").html(generalStats.headshots);
		$("#total_kills_per_minute").html(generalStats.killsPerMinute);
		$("#total_killing_streak").html(generalStats.killStreakBonus);
		$("#total_shots_fired").html(generalStats.shotsFired);
		$("#total_shots_hit").html(generalStats.shotsHit);
		$("#total_skill").html(generalStats.skill);


		$("#time_played").html(getTimeInHours(generalStats.timePlayed));
		$("#score_per_minute").html(generalStats.scorePerMinute);
		$("#score_conquest").html(generalStats.conquestlarge + " (" + ((generalStats.conquestlarge/generalStats.totalScore) * 100).toPrecision(3) +  "%)");
		$("#score_tdm").html(generalStats.teamdeathmatch + " (" + ((generalStats.teamdeathmatch/generalStats.totalScore) * 100).toPrecision(3) +  "%)");
		$("#score_dom").html(generalStats.domination + " (" + ((generalStats.domination/generalStats.totalScore) * 100).toPrecision(3) +  "%)");
		$("#total_score").html(generalStats.totalScore);
		
	}

	</script>




	<div class="container">
		<div class="page-header">
			<div id="main_header_title"><h3>Advanced Battlelog report</h3></div>
			<div id="direct_link"></div>
			<div id="platoon_link"></div>
		</div>
		<div class="row">
			<div class="col-xs-3">
				<div>
					<input type="text" class="form-control" id="battlelog_username" placeholder="Account name">
					<br>
					<div id="battlelog_personas"></div>
				</div>
				<div id="ExtraStatButtons" style="display:none">
					<a data-toggle="collapse" data-target="#overallStatsCollapse" aria-expanded="false" aria-controls="overallStatsCollapse">
						<button class="btn btn-primary btn-xs">Show overall stats</button>
					</a>
					<a data-toggle="collapse" data-target="#kdChartCollapse" aria-expanded="false" aria-controls="kdChartCollapse">
						<button class="btn btn-primary btn-xs">Show charts</button>
					</a>
				</div>
			</div>

			<div class="col-xs-9">

				<div class="collapse" id="overallStatsCollapse">
					<div class="well well-sm">
						<h4>Overall stats</h4>
						<table class="table table-bordered">
							<tr>
								<td id="">Kills</td>
								<td id="total_kills" class="text-right"></td>

								<td id="">Time played</td>
								<td id="time_played" class="text-right"></td>
							</tr>
							<tr>
								<td id="">Deaths</td>
								<td id="total_deaths" class="text-right"></td>

								<td id="">SPM</td>
								<td id="score_per_minute" class="text-right"></td>
								
							</tr>
							<tr>
								<td>Diff</td>
								<td id="total_kill_death_diff" class="text-right"></td>
								
								<td id="">Conquest</td>
								<td id="score_conquest" class="text-right"></td>
							</tr>
							<tr>
								<td id="">K/D</td>
								<td id="total_kd" class="text-right"></td>

								<td id="">TDM</td>
								<td id="score_tdm" class="text-right"></td>
							</tr>
							<tr>
								
								<td id="">Kill/min</td>
								<td id="total_kills_per_minute" class="text-right"></td>

								<td id="">Domination</td>
								<td id="score_dom" class="text-right"></td>
							</tr>
							<tr>
								<td id="">Headshots</td>
								<td id="total_headshots" class="text-right"></td>

								<td id=""><strong>Total</strong></td>
								<td id="total_score" class="text-right"></td>

							</tr>
							<tr>
								<td id="">Accuracy</td>
								<td id="total_accuracy" class="text-right"></td>
							</tr>
							<tr>

								<td id="">Shots fired</td>
								<td id="total_shots_fired" class="text-right"></td>
							</tr>
							<tr>
								<td id="">Shots hit</td>
								<td id="total_shots_hit" class="text-right"></td>
							</tr>
							<tr>
								<td id="">Skill</td>
								<td id="total_skill" class="text-right"></td>
							</tr>
						</table>
					</div>
				</div>
				<!-- <div class="well well-sm"> -->
				<table class="table">
					<tr>
						<td id="ratio_summary"></td>
						<td id="kill_death_summary"></td>
						<td id="position_summary"></td>
						<td id="accuracy_summary"> </td>
					</tr>
				</table>
				<!-- </div> -->
			</div>
		<!-- <div class="col-xs-2">
			<div id="ratio_summary"></div>
		</div>
		<div class="col-xs-2">
			<div id="ratio_overall"></div>
		</div>
		<div class="col-xs-2">
			<div id="accuracy_summary"></div>
		</div> -->
	</div>
</div>
<div class="collapse" id="kdChartCollapse">
	<div id="kdChartContainer" style="width:100%; height:250px;"></div>
	<div id="killsDeathsChartContainer" style="width:100%; height:250px;"></div>
	<div id="spmChartContainer" style="width:100%; height:250px;"></div>
	<div id="skillChartContainer" style="width:100%; height:250px;"></div>
	<div id="winAndLoseChartContainer" style="width:100%; height:250px;"></div>
	<div id="positionChartContainer" style="width:100%; height:250px;"></div>
	
</div>
<div class="container">
	<div id="performance"></div>
	<p></p>

	<div id="result">
		<!-- report results goes here... -->
	</div>
	<div id="loadingsign"></div>
	
</div>

<pre id="csv_ratio_chart" style="display:none"></pre>
<pre id="csv_kills_and_deaths_chart" style="display:none"></pre>
<pre id="csv_spm_chart" style="display:none"></pre>
<pre id="csv_skill_chart" style="display:none"></pre>
<pre id="csv_position_chart" style="display:none"></pre>
<pre id="csv_win_and_lose_chart" style="display:none"></pre>

<?php
include 'include/footer.php';
?>