<?php

/**

redirect=%7Cbf4%7C&email=robert%40karlepalm.se&password=*******%21&remember=1&submit=Log+in

http://battlelog.battlefield.com/bf4/warsawbattlereportspopulate/174286254/2048/1/
http://battlelog.battlefield.com/bf4/warsawbattlereportspopulatemore/1113198462/2048/32/1425240235/

Single report details
http://battlelog.battlefield.com/bf4/battlereport/show/32/572125380534406656/1113198462/

Overview report
http://battlelog.battlefield.com/bf4/warsawoverviewpopulate/1113198462/32/

http://battlelog.battlefield.com/bf4/battlereport/loadgeneralreport/572125380534406656/32/0/

Weapons
http://battlelog.battlefield.com/bf4/soldier/Skjaar/weapons/1113198462/ps4/

Detailed stats on player
http://battlelog.battlefield.com/bf4/indexstats/1113198462/32/?stats=1&_=1427885695202

1 = pc 
32 = ps4 
64 = xbone

Debug: http://localhost/~karlepalm/battlefield_old/player_report.php?userid=1160687402&platform=32
*/
if (isset($_GET['userid']) && isset($_GET['platform'])) {
	$personaID = $_GET['userid'];
	$platform = $_GET['platform'];
	if ($personaID != null && $personaID != "") {

		$opts = array('http'=>array(
			'method'=>"GET",
			'timeout' => 60,
			'header'=>"X-AjaxNavigation: 1\r\nX-Requested-With: XMLHttpRequest\r\n"));
		$context = stream_context_create($opts);

		// First report page
		$page = file_get_contents('http://battlelog.battlefield.com/bf4/warsawbattlereportspopulate/' . $personaID . '/2048/' . $platform . '/');
		$response = json_decode($page, true);
		unset($response['data']['favGameReportIds']);
		unset($response['data']['favGameReports']);

		// Get overall stats
		$overallStats = file_get_contents('http://battlelog.battlefield.com/bf4/warsawdetailedstatspopulate/' . $personaID . '/' . $platform . '/');
		$overallStats = json_decode($overallStats, true);
		$response['data']['generalStats'] = $overallStats['data']['generalStats'];

		// Populating with report details!

		if (!isset($response['data']['gameReports'])) {
			echo json_encode($response);
			exit();
		}

		require_once('../include/cacheReader.php');
		$cacheReader = CacheReader::getInstance();

		for ($i = 0; $i < sizeof($response['data']['gameReports']); $i++) { 
			$cachePath = '../cachedReport/' . $platform . '/' . gmdate("Y-m-d",$response['data']['gameReports'][$i]['createdAt']) . "/" . $response['data']['gameReports'][$i]['gameReportId'] . "/";
			
			if (!file_exists($cachePath)) {
				mkdir($cachePath, 0777, true);
			}
			$startTime = round(microtime(true) * 1000);
			if (!file_exists($cachePath . $personaID . ".txt")) {
				$reportContent = @file_get_contents('http://battlelog.battlefield.com/bf4/battlereport/loadgeneralreport/' . $response['data']['gameReports'][$i]['gameReportId'] . '/' . $platform . '/' . $personaID . '', false, $context);
				if ($reportContent === false) {
					$response['data']['gameReports'][$i]['source'] = "Timeout!";
					break;
				} else {
					$cachedReport = fopen($cachePath . $personaID . ".txt", "w");
					fwrite($cachedReport, $reportContent);
					fclose($cachedReport);
					$response['data']['gameReports'][$i]['detailedReport'] = json_decode($reportContent, true);
					$response['data']['gameReports'][$i]['source'] = "battlelog";
				}
			} else {
				$response['data']['gameReports'][$i]['detailedReport'] = $cacheReader->readFromCache($cachePath . $personaID . ".txt");
				$response['data']['gameReports'][$i]['size'] = round(filesize($cachePath . $personaID . ".txt") / 1024);
				$response['data']['gameReports'][$i]['source'] = "cache";
			}
			$response['data']['gameReports'][$i]['performace'] = round(microtime(true) * 1000) - $startTime;
		}

		// Adding overview stats.
		// http://battlelog.battlefield.com/bf4/soldier/Skjaar/stats/1113198462/ps4/
		$response['data']['personaName'] = $response['data']['gameReports'][0]['detailedReport']['playerReport']['persona']['personaName'];
		$overview = file_get_contents('http://battlelog.battlefield.com/bf4/soldier' . '/' . $response['data']['personaName'] . '/' . 'stats/' . $response['data']['personaId'] . '/' . getPlatformAsText($response['data']['platform']) . '/' . '/', false, $context);
		$overview = json_decode($overview, true);
		$response['data']['overview'] = $overview;

		echo json_encode($response);
	}
} else {
	echo '{"type" : "error" , "message" : "userId does not exsist."}';
}

?>

<?php

function getPlatformAsText($platform) {
	switch ($platform) {
		case '1':
		return 'pc';
		case '32':
		return 'ps4';
		case '64':
		return 'xbone';
		default:
		return 'NA';
	}
}

?>