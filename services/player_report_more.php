<?php

/**
Purre
userid: 2955061302371005540
personaid: 1160687402

Skjaar
userid: 2832660534632513271
personaId: 1113198462

http://battlelog.battlefield.com/bf4/warsawbattlereportspopulate/1113198462/2048/32/
http://battlelog.battlefield.com/bf4/warsawbattlereportspopulatemore/1113198462/2048/32/1425240235/

Single report details
http://battlelog.battlefield.com/bf4/battlereport/show/32/572125380534406656/1113198462/

Overview report
http://battlelog.battlefield.com/bf4/warsawoverviewpopulate/1113198462/32/

http://battlelog.battlefield.com/bf4/battlereport/loadgeneralreport/572125380534406656/32/0/
*/
if (isset($_GET['userid']) && isset($_GET['platform']) && isset($_GET['lastGame'])) {
	$personaID = $_GET['userid'];
	$lastGame = $_GET['lastGame'];
	$platform = $_GET['platform'];
	if ($personaID != null && $personaID != "" && $lastGame != null && $lastGame != "") {

		$pageMore = file_get_contents('http://battlelog.battlefield.com/bf4/warsawbattlereportspopulatemore/' . $personaID . '/2048/' . $platform . '/' . $lastGame . '/');
		$response = json_decode($pageMore, true);

		require_once('include/cacheReader.php');
		$cacheReader = CacheReader::getInstance();

		// Populating with report details!
		for ($i = 0; $i < sizeof($response['data']['gameReports']); $i++) { 
			$cachePath = '../cachedReport/' . $platform . '/' . gmdate("Y-m-d",$response['data']['gameReports'][$i]['createdAt']) . "/" . $response['data']['gameReports'][$i]['gameReportId'] . "/";
			
			if (!file_exists($cachePath)) {
				mkdir($cachePath, 0777, true);
			}
			$startTime = round(microtime(true) * 1000);
			if (!file_exists($cachePath . $personaID . ".txt")) {
				$reportContent = file_get_contents('http://battlelog.battlefield.com/bf4/battlereport/loadgeneralreport/' . $response['data']['gameReports'][$i]['gameReportId'] . '/' . $platform . '/' . $personaID . '');
				$cachedReport = fopen($cachePath . $personaID . ".txt", "w");
				fwrite($cachedReport, $reportContent);
				fclose($cachedReport);
				$response['data']['gameReports'][$i]['detailedReport'] = json_decode($reportContent, true);
				$response['data']['gameReports'][$i]['source'] = "battlelog";
			} else {
				$response['data']['gameReports'][$i]['detailedReport'] = $cacheReader->readFromCache($cachePath . $personaID . ".txt");
				$response['data']['gameReports'][$i]['size'] = round(filesize($cachePath . $personaID . ".txt") / 1024);
				$response['data']['gameReports'][$i]['source'] = "cache";
			}
			$response['data']['gameReports'][$i]['performace'] = round(microtime(true) * 1000) - $startTime;
		}

		echo json_encode($response);
	} else {
		echo '{"type" : "error" , "message" : "System error."}';
	}
} else {
	echo '{"type" : "error" , "message" : "System error."}';
}

?>