<?php

// http://battlelog.battlefield.com/bf4/platoons/reports/3822819502589911924/

if (!isset($_GET['platoonid']) && $_GET['platoonid'] != "" ) {
	echo '{"Message": "Platoon Id needs is required."}';
	exit();
}

$platoon_url = 'http://battlelog.battlefield.com/bf4/platoons/reports/' . $_GET['platoonid'] . '/';

$opts = array('http'=>array('method'=>"GET",'header'=>"X-AjaxNavigation: 1\r\nX-Requested-With: XMLHttpRequest\r\n"));
$context = stream_context_create($opts);

$platoon_report_overview = json_decode(file_get_contents($platoon_url, false, $context), true);

$report_list = $platoon_report_overview['context']['reports'];

$platoonMembers = array();

$detailed_report_list = array();


foreach ($report_list as $key => $report_overview) {

	$cache_path = '../cachedReport/' . $report_overview['summary']['platform'] . '/' . gmdate("Y-m-d",$report_overview['summary']['createdAt']) . "/" . $report_overview['summary']['reportId'] . "/";

	if (!file_exists($cache_path)) {
		mkdir($cache_path, 0777, true);
	}

	// echo 'http://battlelog.battlefield.com/' . getGame($report_overview['summary']['game']) . '/battlereport/loadgeneralreport/' . $report_overview['summary']['reportId'] . '/' . $report_overview['summary']['platform'] . '/0/<br>';
	// Create a function that filters only present players. List should only contain neassasary information used by the client.

	if (file_exists($cache_path . 'general_report' . ".txt") || !is_dir_empty($cache_path)) {
		$responseFromCache = json_decode(file_get_contents(getFirstFilenameInDirectory($cache_path)), true);
		$response = filterGameReport($report_overview['userIds'], $responseFromCache);
		$response['fromCache'] = 'true';
		$response['reportOverview'] = $report_overview;
		$detailed_report_list['reports'][] = $response;
	} else {
		$reportContent = file_get_contents('http://battlelog.battlefield.com/' . getGame($report_overview['summary']['game']) . '/battlereport/loadgeneralreport/' . $report_overview['summary']['reportId'] . '/' . $report_overview['summary']['platform'] . '/0/', false, $context);
		$cachedReport = fopen($cache_path . 'general_report' . ".txt", "w");
		fwrite($cachedReport, $reportContent);
		fclose($cachedReport);
		$response = filterGameReport($report_overview['userIds'], json_decode($reportContent, true));
		$response['reportOverview'] = $report_overview;
		$detailed_report_list['reports'][] = $response;
	}
}

$detailed_report_list['platoonMembers'] = getPlatoonMembers($detailed_report_list);
echo json_encode($detailed_report_list);

?>

<?php

function getPlatoonMembers($detailed_report_list) {
	$platoonMemberList = array();
	foreach ($detailed_report_list['reports'] as $key => $report) {
		foreach ($report['members'] as $personaId => $value) {
			if (isset($platoonMemberList[$personaId]) && $platoonMemberList[$personaId]['personaName'] != $value['persona']['personaName']) {
				$platoonMemberList[$personaId]['personaName'] += '/' . $value['persona']['personaName'];
			}
			if (!isset($platoonMemberList[$personaId])) {
				$platoonMemberList[$personaId]['personaName'] = $value['persona']['personaName'];
				$platoonMemberList[$personaId]['personaId'] = $value['persona']['personaId'];
			}
		}		
	}
	return $platoonMemberList;
}

function filterGameReport($platoonMembers, $singelGameReport) {
	$filteredReport = array();
	$memberReports = array();
	$score = array(1 => 0, 2 => 0);
	foreach ($singelGameReport['players'] as $keyPersonaId => $player) {
		$score[$player['team']] += $player['combatScore'];
		if (in_array($player['persona']['userId'], $platoonMembers)) {
			$score['platoonScore'] += $player['combatScore'];
			$scoreHodler['id'] = $keyPersonaId;
			$scoreHodler['score'] = $player['combatScore'];
			$score['memberScore'][] = $scoreHodler;
			$player['personalPrize'] = null;
			foreach ($singelGameReport['allPersonalPrizes'] as $keyPersonaId2 => $prizeObject) {
				if ($keyPersonaId == $keyPersonaId2) {
					$player['personalPrize'] = $prizeObject;
				}
			}
			$memberReports['members'][$keyPersonaId] = $player;
		}
	}
	$memberReports['createdAt'] = $singelGameReport['createdAt'];
	$memberReports['duration'] = $singelGameReport['duration'];
	$memberReports['teams'] = $singelGameReport['teams'];
	$memberReports['teamScore'] = $score;
	return $memberReports;
}

function getGame($game) {
	switch ($game) {
		case 2048:
			return 'bf4';
		case 8192:
			return 'bfh';
		default:
			return 'EPIC_FAIL_ERROR';
	}
}

function getFirstFilenameInDirectory($dir) {
	foreach(new DirectoryIterator($dir) as $file)
	{
		if ($file->isFile()) {
			return $dir . $file;
		}        
	}
}

function is_dir_empty($dir) {
	return (count(glob($dir . '*')) == 0);
}

function getPersonaId($userId, $game) {
	// echo 'http://battlelog.battlefield.com/' . getGame($game) . '/user/overviewBoxStats/' . $userId . '/<br>';
	$soldiersBox = json_decode(file_get_contents('http://battlelog.battlefield.com/' . getGame($game) . '/user/overviewBoxStats/' . $userId . '/'), true);
	foreach ($soldiersBox['data']['soldiersBox'] as $key => $value) {
		if ($value['game'] == $game) {
			$returnValue['personaId'] = $value['persona']['personaId'];
			$returnValue['personaName'] = $value['persona']['personaName'];
			return $returnValue;
		}
	}
}

?>