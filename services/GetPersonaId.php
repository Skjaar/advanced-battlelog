<?php

/**
http://battlelog.battlefield.com/bf4/user/Skjaar/
http://battlelog.battlefield.com/bf4/user/Disco%20Magik/
*/

if (!isset($_GET["username"])) {
	echo '{"error" : {"message" : "User name cannot be empty."}}';
	return;
}
if ($_GET["username"] == "") {
	echo '{"error" : {"message" : ""}}';
	return;
}

$user_name = $_GET["username"];

$opts = array('http'=>array('method'=>"GET",'header'=>"X-AjaxNavigation: 1\r\n"));
$context = stream_context_create($opts);

$page = file_get_contents('http://battlelog.battlefield.com/bf4/user/' . $user_name . '/', false , $context);

$battlelogResponse = json_decode($page, true);
$response = array();
if (!isset($battlelogResponse['context']['soldiersBox'])) {
	echo '{"error" : {"message" : "No user with the name <strong>' . $user_name . '</strong> was found on Battlelog."}}';
	return;
}
foreach ($battlelogResponse['context']['soldiersBox'] as $key => $value) {
	if ($value['game'] == '2048') {
		$row['game'] = getGameAsText($value['game']);
		$row['personaName'] = $value['persona']['personaName'];
		$row['personaId'] = $value['persona']['personaId'];
		$row['platform'] = getPlatformAsText($value['platform']);
		$response['personaId'][] = $row;	
	}
	
}
$response['error'] = null;
echo json_encode($response);

?>

<?php

function getPlatformAsText($platform) {
	switch ($platform) {
		case '1':
			return 'pc';
		case '2':
			return 'Xbox360';
		case '4':
			return 'PS3';
		case '32':
			return 'PS4';
		case '64':
			return 'Xbox1';
		default:
			return $platform;
	}
}

function getGameAsText($game) {
	switch ($game) {
		case '2':
			return 'BF3';
		case '2048':
			return 'BF4';
		case '8192':
			return 'BFH';
		default:
			return $game;
	}
}

?>