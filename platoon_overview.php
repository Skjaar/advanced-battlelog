<?php 
include 'include/header.php';
?>

	<script type="text/javascript">

	$(function () {

		var platoonId = getUrlVars("platoonid");
		if (platoonId != "" && platoonId != "") { 
			getOverview(platoonId);
		}
	
	});

	function getOverview(platoonId) {
		$("#loadingsign").html('<!--<h3 class="text-center">- Loading content -</h3>--><br><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>');
		$.ajax({
			type: 'get',
			data: {	'platoonid' : platoonId},
			url: 'services/get_platoon_reports.php',
			contentType: "application/json",
			accept: "application/json",
			dataType: "json",
			success: handleReports
		})
	}

	function handleReports(theData) {
		console.log(theData);
		createSimpleTable(theData);
		$('[data-toggle="popover"]').popover(); 
		$("#loadingsign").html("");
	}

	function createSimpleTable(theData) {

		var columnHeaders = ['K','D','K/D','<div class="hide-show">S</div><div class="hide-show" style="display:none;">P</div>', /*'P',*/'Score'];
		var columnSpan = columnHeaders.length;
		var table = '<table class="table table-bordered">';
		var numberOfMembers = 0;

		// make headers
		table += '<tr><th></th><th></th>';
		var names = "";
		for (var key in theData.platoonMembers) {
			if (theData.platoonMembers.hasOwnProperty(key)) {
				if (names != "") {
					names += ",";
				}
				++numberOfMembers;
				table += '<th class="text-center" id="' + key + '" colspan="' + columnSpan + '">' + theData.platoonMembers[key].personaName + '</th>';
				names += theData.platoonMembers[key].personaName;
			}
		}
		names += "\n";
		$("#csv_ratio_chart").html("game," + names);
		$("#csv_score_chart").html("game," + names);
		$("#csv_kills_chart").html("game," + names);
		table += '</tr>';

		table += '<tr><td></td><td>';
		table += '<button type="button" class="btn btn-primary btn-xs" data-toggle="button" aria-pressed="false" onClick="toggleTableValues()" autocomplete="off">Alt</button>';
		table += '</td>';

		for (var i = 0; i < numberOfMembers; i++) {	
			for (var j = 0; j < columnHeaders.length; j++) {
				table += '<td>' + columnHeaders[j] + '</td>';
			};
		};
		table += '<td><div class="text-center"><b>Score</b></div></td>';
		table += '</tr>';		
		// table content
		
		var timeSpan = 0;
		
		for (var i = 0; i < theData.reports.length; i++) {
			table += '<tr>';
			table += '<td class="text-center">' + pad(i + 1, 2) + ' <br> ' + typeOfGame(theData.reports[i].reportOverview.summary.gameMode) + '</td>';
			table += '<td class="text-center"> ' + getTimeAndDate(theData.reports[i].createdAt) + ' <br> ' + showMap(theData.reports[i].reportOverview.summary.map) + '</td>';
			
			var chartRowDataRatio = pad(i + 1, 2);
			var chartRowDataKills = pad(i + 1, 2);
			var chartRowDataScore = pad(i + 1, 2);

			var platoonScore = 0;
			/* 
			var playerScore = {};
			for (var key in theData.platoonMembers) {
				if (theData.platoonMembers.hasOwnProperty(key)) {
					if (theData.reports[i].members[key] == null) {
						playerScore[key] = 0;	
					} else {
						playerScore[key] = 	theData.reports[i].members[key].combatScore;
					}
				}
			}
			*/
			
			theData.reports[i].teamScore.memberScore.sort(compare);
			
			for (var key in theData.platoonMembers) {
				if (theData.platoonMembers.hasOwnProperty(key)) {

					chartRowDataRatio += ',';
					chartRowDataKills += ',';
					chartRowDataScore += ',';

					if (theData.reports[i].members[key] == null) {
						table += '<td id="' + theData.reports.id +'_' + key + '_kills">' + '<div class="text-success">' + '-' + '</div>' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_deaths">' + '<div class="text-danger">' + '-' + '</div>' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_kd">' + '<div class="text-warning">' + '-' + '</div>' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_skill">' + '-' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_score">' + '<div class="text-info">' + '-' + '</div>' + '</td>';
					} else {
						table += '<td id="' + theData.reports.id +'_' + key + '_kills">' + '<div class="text-success">' + theData.reports[i].members[key].kills + '</div>' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_deaths">' + '<div class="text-danger">' + theData.reports[i].members[key].deaths + '</div>' + '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_kd">' + '<div class="text-warning">' + getKdRatio(theData.reports[i].members[key]) + '</div>'+ '</td>';
						table += '<td id="' + theData.reports.id +'_' + key + '_skill">';
						table += '<div class="hide-show" style="display:none;">' + getPosition(key, theData.reports[i].teamScore.memberScore) + '</div>';
						table += '<div class="hide-show">' 						 + theData.reports[i].members[key].skill + '</div>';
						table += '</td>';
						//table += '<td id="' + theData.reports.id +'_' + key + '_pos">' + theData.reports[i].members[key].skill + '</td>';
						var memberScorePercent = ((theData.reports[i].members[key].combatScore / theData.reports[i].teamScore['platoonScore'])*100).toPrecision(3) + '%'; 
						var memberScore = theData.reports[i].members[key].combatScore;
						table += '<td id="' + theData.reports.id +'_' + key + '_score">';
						table += '<div class="hide-show" style="display:none;"> ' + (theData.reports[i].members[key].personalPrize != null ? '<a href="#" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="' + theData.reports[i].members[key].personalPrize + '">' + memberScorePercent + '</a>' : '' + memberScorePercent) + '</div>';
						table += '<div class="hide-show">' 						  + (theData.reports[i].members[key].personalPrize != null ? '<a href="#" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="' + theData.reports[i].members[key].personalPrize + '">' + memberScore + '</a>' : '' + memberScore) + '</div>';
						table += '</td>';
						// chartRowDataRatio += ((theData.reports[i].members[key].kills / theData.reports[i].members[key].deaths).toPrecision(4) == 'NaN' ? 1 : (theData.reports[i].members[key].kills / theData.reports[i].members[key].deaths).toPrecision(4));
						chartRowDataRatio += getKdRatio(theData.reports[i].members[key]);
						chartRowDataKills += theData.reports[i].members[key].kills;
						chartRowDataScore += theData.reports[i].members[key].combatScore;
						platoonScore += theData.reports[i].members[key].combatScore;
						platoonTeam = theData.reports[i].members[key].team;
					}
				}
			}



			var platoonScoreOfTeamScore = (platoonScore / theData.reports[i]['teamScore'][platoonTeam])*100;
			platoonScoreOfTeamScore = platoonScoreOfTeamScore.toPrecision(platoonScoreOfTeamScore < 10 ? 2 : 3);
			table += '<td>';
			table += '<div class="hide-show" style="display:none;">' + '<div class="text-center">' + platoonScoreOfTeamScore + ' %' + '</div></div>';
			table += '<div class="hide-show">' 					  + '<div class="text-center">' + platoonScore + '' + ''   	     + '</div></div>';
			table += '</td>';

			$('#csv_ratio_chart').append(chartRowDataRatio + '\n');
			$('#csv_kills_chart').append(chartRowDataRatio + '\n');
			$('#csv_score_chart').append(chartRowDataScore + '\n');
			table += '</tr>';

		};

		table += '</table>';
		$("#simpleTable").html(table);
		updateCharts();
	}

	function getPosition(key, scoreList) {
		for (var i = 0; i < scoreList.length; i++) {
			if (scoreList[i].id == key) {
				return i + 1;
			}
		};
		return null;
	}

	function compare(a,b) {
		if (a.score < b.score)
			return 1;
		if (a.score > b.score)
			return -1;
		return 0;
	}

	function toggleTableValues() {
		$(".hide-show").toggle();
		// $(".show-hide").toggle();

	}

	function getKdRatio(member) {
		if (member.kills == 0 && member.deaths == 0) {
			return 1;
		} 
		if (member.kills > 0 && member.deaths == 0) {
			return member.kills;
		}

		var ratioDecimals = 2;

		var ratio = member.kills / member.deaths;
		if (ratio >= 10) {
			ratio = ratio.toPrecision(ratioDecimals + 2);
		} else {
			ratio = ratio >= 1 ? ratio.toPrecision(ratioDecimals + 1) : ratio.toPrecision(ratioDecimals);	
		}
		
		return ratio;
	}

	function updateCharts() {
		$('#kdChartContainer').highcharts({
			data: {
				csv: $("#csv_ratio_chart").html()
			},
			legend: {
				enabled: true
			},
			tooltip: {
				shared: true,
			},
			title: {
				text: 'Kill/Death ratio'
			},
			xAxis: {
				reversed: true
			},
			yAxis: [{
				min: 0
			}]
		});

		$('#scoreChartContainer').highcharts({
			data: {
				csv: $("#csv_score_chart").html()
			},
			legend: {
				enabled: true
			},
			tooltip: {
				shared: true,
			},
			title: {
				text: 'Combat Score'
			},
			xAxis: {
				reversed: true
			}
			// yAxis: [{
			// 	min: 0
			// }]
		});

		$('#scoreAreaChartContainer').highcharts({
			data: {
				csv: $("#csv_score_chart").html()
			},
			chart: {
				type: 'column'
			},
			plotOptions: {
				column: {
					stacking: 'percent'
				},
				series: {
                	//pointWidth: 50
                	 // maxPointWidth: 50
            	}
			},
			legend: {
				enabled: true
			},
			tooltip: {
				pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f})<br/>',
				shared: true,
			},
			title: {
				text: 'Combat Score in %'
			},
			xAxis: {
				reversed: true
			},
			yAxis: [{
				min: 0
			}]
		});

		$('#killsAreaChartContainer').highcharts({
			data: {
				csv: $("#csv_kills_chart").html()
			},
			chart: {
				type: 'column'
			},
			plotOptions: {
				column: {
					stacking: 'percent'
				},
				series: {
                	//pointWidth: 50
                	 // maxPointWidth: 50
            	}
			},
			legend: {
				enabled: true
			},
			tooltip: {
				pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b><br/>',
				shared: true,
			},
			title: {
				text: 'Kills in %'
			},
			xAxis: {
				reversed: true
			},
			yAxis: [{
				min: 0
			}]
		});

	}

	</script>

	<div class="container">
		<div class="page-header">
			<div id="main_header_title"><h3>Advanced Battlelog report</h3></div>
			<div id="direct_link"></div>
		</div>
		<a data-toggle="collapse" data-target="#charts" aria-expanded="false" aria-controls="kdChartCollapse">
			<button class="btn btn-primary btn-xs">Show charts</button>
		</a>
	</div>
	<p></p>
	<div class="collapse" id="charts">
		<div id="kdChartContainer" style="width:100%; height:250px;"></div>
		<div id="killsAreaChartContainer" style="width:100%; height:250px;"></div>
		<div id="scoreChartContainer" style="width:100%; height:250px;"></div>
		<div id="scoreAreaChartContainer" style="width:100%; height:250px;"></div>
		<p></p>
	</div>
	<div class="container">
		<div id="loadingsign"></div>
		<div id="simpleTable"></div>
	</div>


	<script type="text/javascript">

	function getUrlVars(key) {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,arrayKey,value) {
			vars[arrayKey] = value;
		});
		return key == null ? vars : vars[key];
	}

	</script>
	<pre id="csv_ratio_chart" style="display:none"></pre>
	<pre id="csv_score_chart" style="display:none"></pre>
	<pre id="csv_kills_chart" style="display:none"></pre>

<?php
include 'include/footer.php';
?>